import AsyncStorage from '@react-native-async-storage/async-storage';


export enum StoreKey {
    TOKEN = 'TOKEN',
    USER = 'USER',
}

// lưu dữ liệu string
const setItem = async (key: string, value: string) => {
    try {
        await AsyncStorage.setItem(key, value)
    } catch (e) {
        // saving error
        return null;
    }
}

// đọc dữ liệu kiểu string
const getItem = async (key: string) => {
    try {
        const value = await AsyncStorage.getItem(key)
        return value;
    } catch (e) {
        // error reading value
        return null;
    }
}


const AsyncStorageUtils = {
    setItem,
    getItem
}

export default AsyncStorageUtils;