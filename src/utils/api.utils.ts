import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://api.fashion.yez.vn:3000/',
    timeout: 15 * 1000, // đơn vị ms (mili seconds) 1s = 1000 ms
    headers: { 'X-Custom-Header': 'foobar' }
});

// method(phương thức) get
const fetch = (url: string, params?: object) => {
    return instance.get(url, { params });
}

const postFormData = (url: string, formData: object) => {
    return instance.post(url, formData,
        {
            headers: {
                authorization: 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDgxODY1MTMzMzdiMTRjM2M1MmZjYzIiLCJ0eXBlIjoiSURfVE9LRU4iLCJpYXQiOjE2MTkxMDEyNjUsImV4cCI6MTYxOTI3NDA2NX0.zAYyhB9JnIE7CnqnXnhBXU-k0Fj9vnVuKeZGyT4_Rw_N9mas3kEYu8pB2gvGlpjG_a9e57B3pLZ_VGIEjh37c-uFerQv_r3bFmfEJAj3RwpYpZsdzVwn3GMurp1_d9ul00Z8e3nm2z_ktHVFmzr1GZvl33m0IW_zFIz1a5LigjAk4brbKhC8XKks4fRkIPkKDMZ7Fb3gJzD_EA3lYo3rX-LcA8qQ9FwP47HENYM48XuRo-dK9Ad_GCj_3oDu4ZnB-AIH_A89Ln4wGr5z0uJ3LKoo-lA0IJkhIVIyuG3_tQx4pJhTdJJNcQLzbFv0hkQjronRI38FuJi0otwZJyyv2aUvZ4Ok85wBLZXP2qrjPCn2QPKAtJNkMh7idhepomMlKrutPv49eQZX6N8XKdMBAGjHZBHlstgwcHUamKKUohGfyktuhuX5WKEY9kUnFDCC9AQp0zW4Tu64fgKiGGfqljt9RXK5AaTTXARX5i5FgUGZyxZEkYJS7UBzRjq5pqJpfMHlbBF9SAlbeKCnLVa-33YAocCVshBXkwGmzSI-RuADvhOoCHVi4OfS7kvCMgSO3fB6BzeD9u9KzlcHa4b5hcWzXPqy2miaAzi5mvHPX8Vuv80lqIPfMVnwds2sk0aW6Hw3CHONV4k5BvHsS9yBk20AkgCFtSYf6WEhYfVSVnA'
            }
        });
}

// post
// delete
// put

const Apis = {
    fetch,
    postFormData
}

export default Apis;