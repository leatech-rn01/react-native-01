import React from "react";
import { Image, StyleSheet, View, TextInput, TouchableOpacity, Text } from "react-native";
import R from "res";

interface Props { }

export default class HomeHeaderComponent extends React.PureComponent<Props> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inputView}>
                    <Image style={styles.icSearch} source={R.images.icSearch} />
                    <TextInput
                        style={styles.input}
                        placeholder="Tìm kiếm sản phẩm"
                        placeholderTextColor="#A5A6AE"
                    />
                </View>

                <TouchableOpacity style={styles.cartView}>
                    <Image style={styles.icCart} source={R.images.icCart} />
                    <View style={styles.badgeView}>
                        <Text style={styles.badge}>3</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingBottom: 13,
        paddingTop: 40,
        backgroundColor: R.colors.primaryColor
    },
    inputView: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1,
        paddingVertical: 8,
        borderRadius: 30,
        paddingHorizontal: 10
        // height: 34
    },
    icSearch: {
        width: 26,
        height: 26,
        resizeMode: 'contain'
    },
    input: {
        paddingVertical: 0
    },
    cartView: {
        paddingHorizontal: 18,
        paddingVertical: 8
    },
    icCart: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    badgeView: {
        width: 19,
        height: 19,
        borderRadius: 19,
        backgroundColor: '#FF7474',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        right: 5
    },
    badge: {
        textAlign: 'center',
        fontSize: 12,
        color: '#fff'
    },
})