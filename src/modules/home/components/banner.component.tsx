import React from "react";
import { Image, StyleSheet, View, TextInput, TouchableOpacity, Text, Dimensions } from "react-native";
import R from "res";
import Swiper from 'react-native-swiper'

const { width } = Dimensions.get('screen');

interface Props { }

export default class BannerComponent extends React.PureComponent<Props> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.background} />
                <Swiper style={styles.wrapper}
                    dotColor="#ddd"
                    activeDotColor="black"
                    autoplay
                >
                    <TouchableOpacity style={styles.btn}>
                        <Image style={styles.image} source={{ uri: 'http://gg.gg/oz0aw' }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn}>
                        <Image style={styles.image} source={{ uri: 'http://gg.gg/oz0br' }} />
                    </TouchableOpacity>
                </Swiper>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    background: {
        backgroundColor: R.colors.primaryColor,
        height: 100,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        zIndex: 0,
        borderBottomLeftRadius: 14,
        borderBottomRightRadius: 14,
    },
    container: {
        height: 145,
    },
    wrapper: {
        marginLeft: 10
    },
    btn: {
        borderRadius: 7,
        overflow: 'hidden'
    },
    image: {
        height: 145,
        width: width - 20,
        resizeMode: 'cover',
        borderRadius: 7,
    }

})