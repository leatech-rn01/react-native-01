import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import BannerComponent from "./components/banner.component";
import HomeHeaderComponent from "./components/home-header.component";

interface Props {
}


interface State {
    token: string;
}

const { width, height } = Dimensions.get('window');



export default class HomeScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            token: ''
        }
    }

    async componentDidMount() {
        
    }


    render() {
       
        return (
            <View style={styles.container}>
                <HomeHeaderComponent />
                <BannerComponent />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
})