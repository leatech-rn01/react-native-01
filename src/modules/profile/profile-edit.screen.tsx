import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import R from "res";
import { goBack } from "routing/navigation.service";
import ImagePicker from 'react-native-image-crop-picker';
import Apis from "utils/api.utils";

interface Props {
    route: any
}

interface States {
    name: string;
    phone: string;
    avatar?: string;
    gender: Gender;
    city: string;
}

enum Gender {
    MALE,
    FEMALE
}

export default class ProfileEditScreen extends React.Component<Props, States> {
    constructor(props: Props) {
        super(props);
        const { name, phone } = props.route?.params || {}
        this.state = {
            name,
            phone,
            avatar: undefined,
            gender: Gender.MALE,
            city: ''
        }
    }

    goBack = () => {
        goBack()
    }

    onChangeText = (key: string) => (value: string) => {
        // @ts-ignore
        this.setState({ [key]: value })
    }

    onSave = () => {
        const { updateInfo } = this.props.route?.params || {}
        if (updateInfo) {
            updateInfo({ name: this.state.name, phone: this.state.phone })
        }
        goBack();
    }

    onChangeCity = (city: string) => {
        this.setState({ city })
    }

    onOpenPicker = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(async image => {
            console.log('image: ', image);
            if (image && image.path) {
                const formData = new FormData();
                const name = image.path.substring(image.path.lastIndexOf('/') + 1)
                console.log('name: ', name);
                formData.append('file', {
                    uri: image.path,
                    type: image.mime,
                    name
                })
                const res = await Apis.postFormData('medias/upload-cloud', formData)
                console.log('res: ', res);
                if (res && res.data.data.url) {
                    this.setState({ avatar: res.data.data.url})
                }
            }
        });
    }

    setGender = (gender: Gender) => () => {
        this.setState({ gender })
    }

    render() {
        const { avatar, gender } = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.headerView}>
                    <TouchableOpacity onPress={this.goBack} style={styles.btnEdit}>
                        <Image source={R.images.icBack} />
                    </TouchableOpacity>
                    <Text style={styles.title}>Thông tin cá nhân</Text>
                    <TouchableOpacity onPress={this.onSave} style={styles.btnEdit}>
                        <Text style={styles.txtEdit}>Lưu</Text>
                    </TouchableOpacity>
                </View>


                <TouchableOpacity onPress={this.onOpenPicker} style={styles.avatarView}>
                    <Image style={styles.avatar} source={avatar ? { uri: avatar } : R.images.defaultAvatar} />
                    <Image style={styles.camera} source={R.images.icCameara} />
                </TouchableOpacity>

                <View style={styles.item}>
                    <Image style={styles.icon} source={R.images.icUser} />
                    <TextInput onChangeText={this.onChangeText('name')} style={styles.input} value={this.state.name} />
                </View>
                <View style={styles.item}>
                    <Image style={styles.icon} source={R.images.icPhone} />
                    <TextInput onChangeText={this.onChangeText('phone')} style={styles.input} value={this.state.phone} />
                </View>

                <TouchableOpacity onPress={this.setGender(Gender.MALE)}>
                    {/* <Image source={ gender === Gender.MALE ? 'image checked' : 'image not check' } /> */}
                    <Text>Nam</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.setGender(Gender.FEMALE)}>
                    {/* <Image  source={ gender === Gender.FEMALE ? 'image checked' : 'image not check' } /> */}
                    <Text>Nu</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    avatarView: {
        marginTop: 21,
        marginBottom: 29,
        width: 85,
        height: 85,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    avatar: {
        width: 85,
        height: 85,
        resizeMode: 'cover',
        borderRadius: 85 / 2,
    },
    camera: {
        width: 31,
        height: 31,
        resizeMode: 'contain',
        position: 'absolute',
        zIndex: 1,
        bottom: 0,
        right: -10
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    item: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 18,
        paddingHorizontal: 10

    },
    input: {
        flex: 1,
        borderBottomColor: '#ddd',
        borderBottomWidth: 1
    },
    icon: {
        width: 23,
        height: 23,
        resizeMode: 'contain',
        marginRight: 12
    },
    name: {
        fontSize: 14,
        color: '#151515'
    },
    headerView: {
        backgroundColor: R.colors.primaryColor,
        display: 'flex',
        flexDirection: 'row'
    },
    title: {
        textAlign: 'center',
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 15,
        color: '#fff',
        fontSize: 16,
        textTransform: 'uppercase'
    },
    btnEdit: {
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    txtEdit: {
        color: '#fff',
        fontSize: 16
    }
})