import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import R from "res";
import { navigateToPage } from "routing/navigation.service";
import { ProfileEditRouter } from "routing/router.name";
import { InforParam } from "./profile.types";

interface Props { }
interface States {
    name: string;
    phone: string;
}
export default class ProfileScreen extends React.Component<Props, States> {
    constructor(props: Props) {
        super(props);
        this.state = {
            name: 'Le hoang',
            phone: '1234'
        }
    }

    goEdit = () => {
        navigateToPage(ProfileEditRouter, {
            name: this.state.name,
            phone: this.state.phone,
            updateInfo: this.updateInfo
        })
    }


    updateInfo = ({name, phone}: InforParam) => {
        this.setState({ name, phone })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerView}>
                    
                    <Text style={styles.title}>Thông tin cá nhân</Text>
                    <TouchableOpacity onPress={this.goEdit} style={styles.btnEdit}>
                        <Text style={styles.txtEdit}>Sửa</Text>
                    </TouchableOpacity>
                </View>
                <Text>Thông tin tài khoản</Text>
                <View style={styles.item}>
                    <Image style={styles.icon} source={R.images.icUser} />
                    <Text style={styles.name}>{this.state.name}</Text>
                </View>
                <View style={styles.item}>
                    <Image style={styles.icon} source={R.images.icPhone} />
                    <Text style={styles.name}>{this.state.phone}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        // paddingHorizontal: 10
    },
    item: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 18,
        borderBottomColor: '#ddd',
        borderBottomWidth: 1
    },
    icon: {
        width: 23,
        height: 23,
        resizeMode: 'contain',
        marginRight: 12
    },
    name: {
        fontSize: 14,
        color: '#151515'
    },
    headerView: {
        backgroundColor: R.colors.primaryColor,
        display: 'flex',
        flexDirection: 'row'
    },
    title: {
        textAlign: 'center',
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 15,
        color: '#fff',
        fontSize: 16,
        textTransform: 'uppercase'
    },
    btnEdit: {
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    txtEdit: {
        color: '#fff',
        fontSize: 16
    }
})