import React from "react";
import { Image, ImageBackground, StatusBar, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import R from "res";
import { navigateToPage } from "routing/navigation.service";
import { HomeRouter } from "routing/router.name";

interface DataItem {
    bg: number;
    img: number;
    title: string;
    description: string;
}

const Data: DataItem[] = [
    {
        bg: R.images.bgScreen1,
        img: R.images.screen1,
        title: 'THỜI TRANG NAM',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
    },
    {
        bg: R.images.bgScreen2,
        img: R.images.screen2,
        title: 'THỜI TRANG Nữ',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
    },
    {
        bg: R.images.bgScreen3,
        img: R.images.screen3,
        title: 'THỜI TRANG Trẻ em',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
    },
]


interface Props { }
interface States {
    index: number;
}

export default class OnboardingScreen extends React.Component<Props, States> {

    constructor(props: Props) {
        super(props);
        this.state = {
            index: 0
        }

    }

    onNext = () => {
        this.setState(prevState => {
            let index = prevState.index;

            if (index === Data.length - 1) {
                // index = 0;
                navigateToPage(HomeRouter)
            } else {
                index += 1;
            }

            return {
                index
            }
        })
    }

    render() {
        const { index } = this.state;
        const item = Data[index];
        return (
            <ImageBackground
                source={item.bg}
                style={styles.container}
            >
                <StatusBar backgroundColor="transparent" translucent />
                <Image style={styles.image} source={item.img} />

                <View style={styles.dotView}>
                    <View style={[styles.dot, index === 0 && styles.dotAt]} />
                    <View style={[styles.dot, index === 1 && styles.dotAt]} />
                    <View style={[styles.dot, index === 2 && styles.dotAt]} />
                </View>

                <Text>{item.title}</Text>
                <TouchableOpacity
                    onPress={this.onNext}
                    style={styles.btn}
                >
                    <Text style={styles.txt}>Tiếp tục</Text>
                </TouchableOpacity>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        width: 248,
        height: 517,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    btn: {
        width: 233,
        paddingVertical: 10,
        borderRadius: 25,
        alignSelf: 'center',
        backgroundColor: '#FFB79A'
    },
    txt: {
        textAlign: 'center',
        color: '#fff'
    },
    dotView: {
        // borderWidth: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'

    },
    dot: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: '#ddd',
        marginRight: 5
    },
    dotAt: {
        backgroundColor: '#000',
        width: 10,
        height: 10,
        borderRadius: 10
    }
})