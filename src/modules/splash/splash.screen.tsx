import React from "react";
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import { GlobalVariable } from "../../constans/global-varable";
import R from "../../res";
import { navigateToPage } from "../../routing/navigation.service";
import { HomeRouter, OnboardingRouter, SigninRouter } from "../../routing/router.name";
import AsyncStorageUtils, { StoreKey } from "../../utils/async-storage.utils";

interface Props { }
interface States { }
export default class SplashScreen extends React.Component<Props, States> {
    timeout: NodeJS.Timeout;

    async componentDidMount() {
        // check xem đã đăng nhập hay chưa
        const token = await AsyncStorageUtils.getItem(StoreKey.TOKEN);
        console.log('token: ', token);
        GlobalVariable.token = token;

        if (this.timeout) {
            clearTimeout(this.timeout);
        }

        this.timeout = setTimeout(() => {
            // code
            navigateToPage(OnboardingRouter)
            // if (!token) {
            //     // chuyển sang màn đăng nhập
            //     navigateToPage(SigninRouter)
            // } else {
            //     // chuyeenr sang man home
            //     navigateToPage(HomeRouter)
            // }
        }, 2000);

        // !token tức là ko có token; !token === null | undefined

    }

    componentWillUnmount() {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
    }

    render() {
        return (
            <ImageBackground source={R.images.bgSplash} style={styles.container}>
                <Text>Splash Screen</Text>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})