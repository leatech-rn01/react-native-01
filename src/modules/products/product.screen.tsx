import HomeHeaderComponent from "modules/home/components/home-header.component";
import React from "react";
import { FlatList, Text, View, ListRenderItem, StyleSheet, Dimensions, Image, ActivityIndicator, TouchableOpacity } from "react-native";
import R from "res";
import Apis from "utils/api.utils";
import { formatPrice } from "utils/common.utils";
import { FetchProduct, Product } from "./product.types";

const { width } = Dimensions.get('screen');

const padding = 10;
const numcolums = 2;
const itemWidth = (width - padding * (numcolums + 1)) / numcolums

interface Props { }
interface States {
    products: Product[],
    loading: boolean,
    isLoadmore: boolean,
    page: number;
}

export class ProductScreen extends React.Component<Props, States> {
    flatlistRef = React.createRef<FlatList>();

    constructor(props: Props) {
        super(props);
        this.state = {
            products: [],
            loading: true,
            isLoadmore: false,
            page: 1
        }
    }

    async componentDidMount() {
        this.fetchData();
    }

    onPullRefresh = () => {
        this.setState({ page: 1, loading: true }, () => {
            this.fetchData({ page: 1, limit: 20 });
        })
    }

    fetchData = async (params?: FetchProduct) => {
        try {
            let page = this.state.page;
            const res = await Apis.fetch('product', params);
            let products = [...this.state.products];
            if (params && params.page != 1) {
                products = [...products, ...res.data.data];
                page = params.page;
            } else {
                products = [...res.data.data]
            }
            console.log('products: ', products);
            this.setState({ products, loading: false, page, isLoadmore: false })
        } catch (error) {

        }

    }

    loadmoreData = async () => {
        const { page } = this.state;
        this.setState({ isLoadmore: true }, () => {
            this.fetchData({ page: page + 1, limit: 20 })
        })
    }

    onGotoTop = () => {
        console.log('ref', this.flatlistRef);
        if (this.flatlistRef && this.flatlistRef.current) {
            this.flatlistRef.current!.scrollToOffset({ animated: true, offset: 0 })
        }
    }

    renderItem: ListRenderItem<Product> = ({ item }): React.ReactElement => {
        return (
            <View style={styles.itemView}>
                <View style={styles.imageView}>
                    {item.imageInfo && item.imageInfo[0] &&
                        <Image style={styles.image} source={{ uri: item.imageInfo[0].thumbnail }} />
                    }
                </View>
                <View style={styles.content}>
                    <Text style={styles.name}>{item.name}</Text>
                    <Text style={styles.price}>{formatPrice(item.price)}đ</Text>
                    <Text style={styles.originPrice}>{formatPrice(item.originPrice)}đ</Text>
                </View>
            </View>
        )
    }

    keyExtractor = (item: Product) => item._id;

    ListHeaderComponent = () => {
        const { loading } = this.state;
        if (!loading) return null;
        return (
            <View style={styles.loadingView}>
                <ActivityIndicator color="red" size="large" />
            </View>
        )
    }

    ListFooterComponent = () => {
        const { isLoadmore } = this.state;
        if (!isLoadmore) return null;
        return (
            <View style={styles.loadingView}>
                <ActivityIndicator color="blue" size="small" />
            </View>
        )
    }

    render(): React.ReactNode {
        const { products, loading } = this.state;
        return (
            <View style={styles.container}>
                <HomeHeaderComponent />
                <FlatList
                    ref={this.flatlistRef}
                    refreshing={loading}
                    onRefresh={this.onPullRefresh}
                    // ListHeaderComponent={this.ListHeaderComponent}
                    ListFooterComponent={this.ListFooterComponent}
                    data={this.state.products}
                    renderItem={this.renderItem}
                    keyExtractor={this.keyExtractor}
                    numColumns={numcolums}
                    onEndReached={this.loadmoreData}
                    onEndReachedThreshold={0.5}
                />

                <TouchableOpacity onPress={this.onGotoTop} style={styles.btn}>
                    <Image source={R.images.icArrowUp} style={styles.icArrow} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F4F4F4'
    },
    itemView: {
        width: itemWidth,
        borderRadius: 10,
        backgroundColor: '#fff',
        marginLeft: padding,
        marginTop: padding,
        overflow: 'hidden',
        paddingBottom: 10
    },
    imageView: {
        width: '100%',
        height: 192,
        backgroundColor: '#fff',
        paddingVertical: 10
    },
    image: {
        width: '80%',
        height: 192 - 20,
        resizeMode: 'cover',
        alignSelf: 'center'
    },
    content: {
        paddingHorizontal: 8,

    },
    name: {
        fontSize: 14,
        color: '#020306',
        textTransform: 'capitalize'
    },
    originPrice: {
        fontSize: 12,
        color: '#A5A6AE',
        textDecorationLine: 'line-through'
    },
    price: {
        fontSize: 18,
        color: '#FF7474',
        marginTop: 17,
        marginBottom: 6
    },
    loadingView: {
        paddingTop: 10
    },
    icArrow: {

    },
    btn: {
        position: 'absolute',
        bottom: 30,
        right: 10
    }
})