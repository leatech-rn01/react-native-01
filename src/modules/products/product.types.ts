export interface Media {
    url: string;
    thumbnail: string;
    width: number;
    height: number;
}

export interface Product {
    _id: string;
    price: number;
    originPrice: number;
    description: string;
    name: string;
    imageInfo: Media[]
}

export interface FetchProduct {
    limit: number;
    page: number;
}