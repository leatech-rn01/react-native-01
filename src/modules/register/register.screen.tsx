/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import { HeaderComponent } from 'components/header.component';
import React from 'react';
import {
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native';
import { InputComponent } from 'components/input.component';
import { navigateToPage } from 'routing/navigation.service';
import { SigninRouter } from 'routing/router.name';

interface Props {

}

// class lifecircle
class RegisterScreen extends React.Component<Props> {
    fullnameRef = React.createRef<InputComponent>();
    phoneRef = React.createRef<InputComponent>();

    onRegister = () => {
        const fullname = this.fullnameRef.current!.state.value;
        const phone = this.phoneRef.current!.state.value;
        console.log('fullname: ', fullname);
        console.log('phone: ', phone);
    }

    onLogin = () => {
        console.log('bem');
        navigateToPage(SigninRouter)
    }

    hideKeyboard = () => {
        Keyboard.dismiss()
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={this.hideKeyboard} style={styles.flex}>
                <View style={styles.container}>
                    <StatusBar backgroundColor="#FFB79A" barStyle="light-content" />

                    <HeaderComponent title="Đăng ký" />

                    <View style={styles.content}>
                        <InputComponent ref={this.fullnameRef} title="Họ và tên" />
                        <InputComponent ref={this.phoneRef} title="Số điện thoại" />
                        <InputComponent title="Email" />
                        <InputComponent title="Mật khẩu" />

                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.btn}
                            onPress={this.onRegister}
                        >
                            <Text style={styles.txt}>Đăng ký</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.txtLoginView}>
                        Đã có tài khoản?
             {' '}
                        <Text onPress={this.onLogin} style={styles.txtLogin}>Đăng nhập</Text>
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }

};

const styles = StyleSheet.create({
    flex: {
        flex: 1
    },
    content: {
        flex: 1,
        marginTop: -20,
        borderTopLeftRadius: 20,
        overflow: 'hidden',
        paddingTop: 28
    },

    container: {
        flex: 1,
        backgroundColor: 'white',
    },



    btn: {
        backgroundColor: '#FFB79A',
        height: 49,
        marginHorizontal: 40,
        borderRadius: 25,
        justifyContent: 'center'
    },
    txt: {
        textAlign: 'center',
        color: 'white'
    },
    inputView: {
        marginHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 23,

    },
    label: {
        fontSize: 12,
        color: '#A5A6AE'
    },
    txtLoginView: {
        color: '#707070',
        textAlign: 'center',
        paddingVertical: 33,
        fontSize: 15
    },
    txtLogin: {
        color: '#020306',
        fontWeight: 'bold',
    }
});

export default RegisterScreen;
