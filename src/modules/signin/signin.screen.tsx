/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import { HeaderComponent } from '../../components/header.component';
import { InputComponent } from '../../components/input.component';
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { navigateToPage, resetStack } from '../../routing/navigation.service';
import { HomeRouter, RegisterRouter } from '../../routing/router.name';
import AsyncStorageUtils, { StoreKey } from '../../utils/async-storage.utils';

interface Props {

}

// class lifecircle
class SigninScreen extends React.Component<Props> {
    fullnameRef = React.createRef<InputComponent>();
    phoneRef = React.createRef<InputComponent>();

    // componentDidMount() {
    //     console.log('componentDidMount chạy');
    // }

    // componentWillUnmount() {
    //     console.log('componentWillUnmount chạy');
    // }

    onRegister = () => {
        // const fullname = this.fullnameRef.current?.state.value;
        // const phone = this.phoneRef.current?.state.value;
        // console.log('fullname: ', fullname);
        // console.log('phone: ', phone);
        navigateToPage(RegisterRouter)
    }

    onLogin = async () => {
        console.log('bem');
        // todo: Gọi api đăng nhập
        await AsyncStorageUtils.setItem(StoreKey.TOKEN, 'tesssss');
        // navigateToPage(HomeRouter)
        resetStack(HomeRouter)
    }

    onLoginFb = () => {
        console.log('Đăng nhập với facebook');
        LoginManager.logInWithPermissions(["public_profile"]).then(
            function (result) {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    // thực thi code ở đây
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            console.log('bem', data.accessToken.toString())
                        }
                    )
                }
            },
            function (error) {
                console.log("Login fail with error: " + error);
            }
        );

    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#FFB79A" barStyle="light-content" />

                <HeaderComponent title="Đăng nhập" />

                <View style={styles.content}>
                    <InputComponent ref={this.phoneRef} title="Số điện thoại" />
                    <InputComponent title="Mật khẩu" secureTextEntry />

                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.btn}
                        onPress={this.onLogin}
                    >
                        <Text style={styles.txt}>Đăng nhập</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={[styles.btn, styles.btnFb]}
                        onPress={this.onLoginFb}
                    >
                        <Text style={styles.txt}>Đăng nhập Facebook</Text>
                    </TouchableOpacity>
                </View>
                <Text style={styles.txtLoginView}>
                    Chưa có tài khoản?
             {' '}
                    <Text onPress={this.onRegister} style={styles.txtLogin}>Đăng ký</Text>
                </Text>
            </View>
        );
    }

};

const styles = StyleSheet.create({
    content: {
        flex: 1,
        marginTop: -20,
        borderTopLeftRadius: 20,
        overflow: 'hidden',
        paddingTop: 28
    },

    container: {
        flex: 1,
        backgroundColor: 'white',
    },

    btn: {
        backgroundColor: '#FFB79A',
        height: 49,
        marginHorizontal: 40,
        borderRadius: 25,
        justifyContent: 'center'
    },
    btnFb: {
        backgroundColor: 'red',
        marginTop: 10
    },
    txt: {
        textAlign: 'center',
        color: 'white'
    },
    inputView: {
        marginHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 23,

    },
    label: {
        fontSize: 12,
        color: '#A5A6AE'
    },
    txtLoginView: {
        color: '#707070',
        textAlign: 'center',
        paddingVertical: 33,
        fontSize: 15
    },
    txtLogin: {
        color: '#020306',
        fontWeight: 'bold',
    }
});

export default SigninScreen;
