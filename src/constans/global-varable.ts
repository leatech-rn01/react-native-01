
interface Global {
    token: string | null;
    user?: any;
}

export const GlobalVariable: Global = {
    token: null,
    user: undefined
}