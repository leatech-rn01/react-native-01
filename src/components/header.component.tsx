import React from "react";
import { Image, StatusBar, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import R from "../res";
import { goBack } from "../routing/navigation.service";

interface Props {
    title: String;
}

export class HeaderComponent extends React.Component<Props> {
    render() {
        return (
            <View style={styles.header}>
                <TouchableOpacity
                    onPress={goBack}
                    style={styles.btn}
                >
                    <Image style={styles.icBack} source={R.images.icBack} />
                </TouchableOpacity>
                <Text style={styles.txtHeader}>{this.props.title}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#FFB79A',
        // justifyContent: 'center',
        paddingTop: StatusBar.currentHeight,
        paddingBottom: 10 + 20,
        alignItems: 'center'
    },
    txtHeader: {
        textAlign: 'center',
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 18,
        width: '100%'
    },
    icBack: {
        width: 26,
        height: 26,
        resizeMode: 'contain'
    },
    btn: {
        position: 'absolute',
        left: 0,
        top: 10,
        zIndex: 10,
        padding: 10,
    }
})