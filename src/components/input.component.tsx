import React from "react";
import { Image, StyleSheet, Text, TextInput, View } from "react-native";

interface Props {
    title: string;
    secureTextEntry?: boolean
}

interface States {
    value: string;
}

export class InputComponent extends React.PureComponent<Props, States> {
    constructor(props: Props) {
        super(props);
        this.state = {
            value: ''
        }
    }

    onChangeText = (value: string) => this.setState({ value })

    render() {
        return (
            <View style={styles.inputView}>
                <Text style={styles.label}>{this.props.title}</Text>
                <TextInput  
                    value={this.state.value}
                    style={styles.input} 
                    onChangeText={this.onChangeText}
                    secureTextEntry={this.props.secureTextEntry}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputView: {
        marginHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 23,

    },
    label: {
        fontSize: 12,
        color: '#A5A6AE'
    },
    input: {

    }
})