import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import RegisterScreen from '../modules/register/register.screen';
import SigninScreen from '../modules/signin/signin.screen';
import * as screen from './router.name';
import SplashScreen from '../modules/splash/splash.screen';
import HomeBottomTabs from './home-bottom-tab';
import OnboardingScreen from 'modules/splash/onboarding.screen';
import ProfileEditScreen from 'modules/profile/profile-edit.screen';

const Stack = createStackNavigator();

export default function MainStack() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
            initialRouteName={screen.HomeRouter}
        >
            <Stack.Screen name={screen.OnboardingRouter} component={OnboardingScreen} />
            <Stack.Screen name={screen.SigninRouter} component={SigninScreen} />
            <Stack.Screen name={screen.RegisterRouter} component={RegisterScreen} />
            <Stack.Screen name={screen.HomeRouter} component={HomeBottomTabs} />
            <Stack.Screen name={screen.SplashRouter} component={SplashScreen} />
            <Stack.Screen name={screen.ProfileEditRouter} component={ProfileEditScreen} />
        </Stack.Navigator>
    )
}