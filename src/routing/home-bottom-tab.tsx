import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from 'modules/home/home.screen';
import NewsScreen from 'modules/news/news.screen';
import NotificationScreen from 'modules/notification/notification.screen';
import { ProductScreen } from 'modules/products/product.screen';
import ProfileScreen from 'modules/profile/profile.screen';
import React from 'react';
import { Image, StyleSheet, TouchableOpacity } from 'react-native';
import R from 'res';


const Tab = createBottomTabNavigator();

export const BottomTabKey = {
  HOME: 'HOME_TAB',
  NEWS: 'NEWS_TAB',
  NOTIFY: 'NOTIFY_TAB',
  PROFILE: 'PROFILE_TAB',
  PRODUCT: 'PRODUCT_TAB',
}

const ScreenOptions = ({ route }: any): any => ({
  tabBarIcon: ({ focused, color, size }: any): any => {
    let iconName;
    let isShowCountNoti = false;

    // @ts-ignore
    switch (route.name) {
      case BottomTabKey.HOME:
        iconName = R.images.icHomeTab
        break;
      case BottomTabKey.NEWS:
        iconName = R.images.icNewsTab
        break;
      case BottomTabKey.NOTIFY:
        iconName = R.images.icNotifyTab
        break;
      case BottomTabKey.PROFILE:
        iconName = R.images.icUserTab
        break;
      case BottomTabKey.PRODUCT:
        iconName = R.images.icProductTab
        break;

      default:
        break;
    }
    return (
      <Image
        resizeMode="contain"
        style={[
          styles.iconTabStyle,
          {
            tintColor: focused ? R.colors.primaryColor : 'black'
          }
        ]}
        source={iconName}
      />

    );
  }
});

export default function HomeBottomTabs() {
  return (
    <Tab.Navigator tabBarOptions={{ showLabel: false }}>
      <Tab.Screen
        options={ScreenOptions}
        name={BottomTabKey.PRODUCT}
        component={ProductScreen}
      />
      <Tab.Screen
        options={ScreenOptions}
        name={BottomTabKey.HOME}
        component={HomeScreen}
      />

      <Tab.Screen
        options={ScreenOptions}
        name={BottomTabKey.NEWS}
        component={NewsScreen}
      />


      <Tab.Screen
        options={ScreenOptions}
        name={BottomTabKey.NOTIFY}
        component={NotificationScreen}
      />

      <Tab.Screen
        options={ScreenOptions}
        name={BottomTabKey.PROFILE}
        component={ProfileScreen}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  iconView: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    alignSelf: 'center'
  },
  iconTabStyle: {
    width: 45,
    height: 45
  }
});
