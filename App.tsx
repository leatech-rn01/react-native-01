import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import MainStack from './src/routing/main-navigation';
import { navigationRef } from './src/routing/navigation.service';

interface Props {}

// class lifecycle
class App extends React.Component<Props> {
  render() {
    return (
      <NavigationContainer
        independent={true}
        ref={navigationRef}
      >
       <MainStack />
      </NavigationContainer>
    );
  }
};

export default App;
